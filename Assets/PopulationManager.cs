﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using System.Text.RegularExpressions;


public class Person
{
	string m_firstName;
	string m_lastName;

	public int m_startDate { get; }
	public int m_endDate { get; } 

	/// <summary>
	/// Create a person with a first and last name, as well as start and end date
	/// </summary>
	public Person (string first, string last, int start, int end)
	{
		m_firstName = first;
		m_lastName = last;
		m_startDate = start;
		m_endDate = end;
	}

	/// <summary>
	/// Returns true if the person was alive during the year to check, otherwise return false.
	/// </summary>
	public bool WasPersonAliveDuringYear(int yearToCheck)
	{
		//Check if this year is between the start and end years
		if(yearToCheck >= m_startDate && yearToCheck <= m_endDate)
			return true;
		return false;
	}
}


public class PopulationManager : MonoBehaviour {
	
	//List of all parsed people
	List<Person> m_allPeopleList = new List<Person>();

	//Year to start checking population
	int m_startYearInt = 1900;
	//Year to end checking population
	int m_endYearInt = 2000;

	//String that contains all persons alive and their start/end dates
	string m_allPeopleInfoString =  "John Doe (1925 - 1980), Jane Doe (1901 - 1934), John Doe (1955 - 1990), Jane Doe (1919 - 1955), John Doe (1907 - 1955), " +
									"John Doe (1915 - 2000), Jane Doe (1942 - 1978), John Doe (1977 - 2000), Jane Doe (1918 - 1982), John Doe (1931 - 1981), " +
									"John Doe (1902 - 1924), Jane Doe (1937 - 1997), John Doe (1976 - 1999), Jane Doe (1913 - 1951), John Doe (1945 - 2000), " +
									"John Doe (1979 - 1992), Jane Doe (1915 - 2000), John Doe (1955 - 1981), Jane Doe (1920 - 1976), John Doe (1929 - 1993)";

	void Start()
	{
		ParseList();
		FindMostPopulatedYear();
	}


	/// <summary>
	/// Parses the list of people, each into the class Person, and stores it in m_allPeopleList
	/// </summary>
	void ParseList()
	{
		//Split all people by ", "
		string[] peopleStringArray = m_allPeopleInfoString.Split(new string[]{", "}, StringSplitOptions.RemoveEmptyEntries);

		//Parse each person's string
		foreach(string s in peopleStringArray) 
		{
			//Seperate the name string out
			string[] nameStrings = s.Substring(0, s.IndexOf(" (")).Split(new string[]{" "}, StringSplitOptions.RemoveEmptyEntries);

			//Take the date info out of the parenthesis
			string parenthesisPattern = @"\(([^)]*)\)";
			Match matches = Regex.Match(s, parenthesisPattern);

			//Fill in person's data
			string firstName = nameStrings[0].Trim();
			string lastName = nameStrings[1].Trim();
			int startDate = int.Parse(matches.Value.Substring(1, 4));
			int endDate = int.Parse(matches.Value.Substring(8, 4));

			//Add to list
			m_allPeopleList.Add(new Person(firstName, lastName, startDate, endDate));
		}
	} 

	/// <summary>
	/// Check each year's population and return the largest population year
	/// </summary>
	int FindMostPopulatedYear()
	{
		int highestPopYear = 1900;
		int highestPop = 0;

		//Check each year's population
		for(int i = m_startYearInt; i <= m_endYearInt; i++)
		{
			int numPopulationThisYear = 0;

			//Check each person to see if they were alive during that year
			foreach(Person p in m_allPeopleList)
			{
				if(p.WasPersonAliveDuringYear(i) == true)
					numPopulationThisYear++;
			}

			//If population of this year is higher than the largest pop year, set this year as the largest
			if(numPopulationThisYear > highestPop)
			{
				highestPopYear = i;
				highestPop = numPopulationThisYear;
			}

			Debug.Log("Year: " + i + "  / Pop: " + numPopulationThisYear);
		}

		Debug.Log("** Highest Population Year: " + highestPopYear + " (Population: " + highestPop + ") **");
		return highestPopYear;
	}

}
