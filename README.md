# README #

### What is this repository for? ###

1. A code test for Dylan Burke to show C# coding skills
2. This code finds the most highly populated year of all years between 1900 and 2000 from a given list of people

3. This is the list that I created for the test:

*  "John Doe (1925 - 1980), Jane Doe (1901 - 1934), John Doe (1955 - 1990), Jane Doe (1919 - 1955), John Doe (1907 - 1955), " + 
*  "John Doe (1915 - 2000), Jane Doe (1942 - 1978), John Doe (1977 - 2000), Jane Doe (1918 - 1982), John Doe (1931 - 1981), " + 
*  "John Doe (1902 - 1924), Jane Doe (1937 - 1997), John Doe (1976 - 1999), Jane Doe (1913 - 1951), John Doe (1945 - 2000), " + 
*  "John Doe (1979 - 1992), Jane Doe (1915 - 2000), John Doe (1955 - 1981), Jane Doe (1920 - 1976), John Doe (1929 - 1993)";

### How to Display Results ###

1. Open The scene in Unity and press play
2. In the console window it will display the following:


*  Year: 1900  / Pop: 0 
*  Year: 1901  / Pop: 1
*  Year: 1902  / Pop: 2
*  Year: 1903  / Pop: 2
*  Year: 1904  / Pop: 2
*  Year: 1905  / Pop: 2
*  Year: 1906  / Pop: 2
*  Year: 1907  / Pop: 3
*  Year: 1908  / Pop: 3
*  Year: 1909  / Pop: 3
*  Year: 1910  / Pop: 3
*  Year: 1911  / Pop: 3
*  Year: 1912  / Pop: 3
*  Year: 1913  / Pop: 4
*  Year: 1914  / Pop: 4
*  Year: 1915  / Pop: 6
*  Year: 1916  / Pop: 6
*  Year: 1917  / Pop: 6
*  Year: 1918  / Pop: 7
*  Year: 1919  / Pop: 8
*  Year: 1920  / Pop: 9
*  Year: 1921  / Pop: 9
*  Year: 1922  / Pop: 9
*  Year: 1923  / Pop: 9
*  Year: 1924  / Pop: 9
*  Year: 1925  / Pop: 9
*  Year: 1926  / Pop: 9
*  Year: 1927  / Pop: 9
*  Year: 1928  / Pop: 9
*  Year: 1929  / Pop: 10
*  Year: 1930  / Pop: 10
*  Year: 1931  / Pop: 11
*  Year: 1932  / Pop: 11
*  Year: 1933  / Pop: 11
*  Year: 1934  / Pop: 11
*  Year: 1935  / Pop: 10
*  Year: 1936  / Pop: 10
*  Year: 1937  / Pop: 11
*  Year: 1938  / Pop: 11
*  Year: 1939  / Pop: 11
*  Year: 1940  / Pop: 11
*  Year: 1941  / Pop: 11
*  Year: 1942  / Pop: 12
*  Year: 1943  / Pop: 12
*  Year: 1944  / Pop: 12
*  Year: 1945  / Pop: 13
*  Year: 1946  / Pop: 13
*  Year: 1947  / Pop: 13
*  Year: 1948  / Pop: 13
*  Year: 1949  / Pop: 13
*  Year: 1950  / Pop: 13
*  Year: 1951  / Pop: 13
*  Year: 1952  / Pop: 12
*  Year: 1953  / Pop: 12
*  Year: 1954  / Pop: 12
*  Year: 1955  / Pop: 14
*  Year: 1956  / Pop: 12
*  Year: 1957  / Pop: 12
*  Year: 1958  / Pop: 12
*  Year: 1959  / Pop: 12
*  Year: 1960  / Pop: 12
*  Year: 1961  / Pop: 12
*  Year: 1962  / Pop: 12
*  Year: 1963  / Pop: 12
*  Year: 1964  / Pop: 12
*  Year: 1965  / Pop: 12
*  Year: 1966  / Pop: 12
*  Year: 1967  / Pop: 12
*  Year: 1968  / Pop: 12
*  Year: 1969  / Pop: 12
*  Year: 1970  / Pop: 12
*  Year: 1971  / Pop: 12
*  Year: 1972  / Pop: 12
*  Year: 1973  / Pop: 12
*  Year: 1974  / Pop: 12
*  Year: 1975  / Pop: 12
*  Year: 1976  / Pop: 13
*  Year: 1977  / Pop: 13
*  Year: 1978  / Pop: 13
*  Year: 1979  / Pop: 13
*  Year: 1980  / Pop: 13
*  Year: 1981  / Pop: 12
*  Year: 1982  / Pop: 10
*  Year: 1983  / Pop: 9
*  Year: 1984  / Pop: 9
*  Year: 1985  / Pop: 9
*  Year: 1986  / Pop: 9
*  Year: 1987  / Pop: 9
*  Year: 1988  / Pop: 9
*  Year: 1989  / Pop: 9
*  Year: 1990  / Pop: 9
*  Year: 1991  / Pop: 8
*  Year: 1992  / Pop: 8
*  Year: 1993  / Pop: 7
*  Year: 1994  / Pop: 6
*  Year: 1995  / Pop: 6
*  Year: 1996  / Pop: 6
*  Year: 1997  / Pop: 6
*  Year: 1998  / Pop: 5
*  Year: 1999  / Pop: 5
*  Year: 2000  / Pop: 4

and Finally!

** * * Highest Population Year: 1955 (Population: 14) * * **